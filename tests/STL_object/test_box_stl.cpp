#include <memory>
#include "STL/stl_object/ObjectBox.hpp"
#include "STL/Parser/Parser.hpp"
#include "gtest/gtest.h"

static const constexpr float EPSILON = 10E-6;

class obj_box_test: public ::testing::Test
{
protected:
    static const inline std::string path_deco = "../../bank_stl/Decorations/";
    std::shared_ptr<ObjectBox> box;
    std::shared_ptr<ObjectStl> stl_obj =
            stl::Parser::parse( path_deco + "tetra_ascii.stl");

    void SetUp() override
    {
        box = std::make_shared<ObjectBox>(*stl_obj);
    }
};

TEST_F(obj_box_test, init)
{
    Point3 test = box->get_center();

    EXPECT_EQ(box->get_max(), Point3(10, 10, 10));
    EXPECT_EQ(box->get_min(), Point3(0, 0, 0));
    EXPECT_EQ(box->get_center(), Point3(5, 5, 5));
}


TEST_F(obj_box_test, translate)
{
    box->translate(10, 0, 0);
    EXPECT_EQ(box->get_max(), Point3(20, 10, 10));
    EXPECT_EQ(box->get_min(), Point3(10, 0, 0));
    EXPECT_EQ(box->get_center(), Point3(15, 5, 5));
}


TEST_F(obj_box_test, rotate)
{
    box->rotate_z(-180, true);
    // max
    EXPECT_NEAR(box->get_max().x(), 10, EPSILON);
    EXPECT_NEAR(box->get_max().y(), 10, EPSILON);
    EXPECT_NEAR(box->get_max().z(), 10, EPSILON);
    // min
    EXPECT_NEAR(box->get_min().x(), 0, EPSILON);
    EXPECT_NEAR(box->get_min().y(), 0, EPSILON);
    EXPECT_NEAR(box->get_min().z(), 0, EPSILON);
    // center
    EXPECT_NEAR(box->get_center().x(), 5, EPSILON);
    EXPECT_NEAR(box->get_center().y(), 5, EPSILON);
    EXPECT_NEAR(box->get_center().z(), 5, EPSILON);
}

TEST_F(obj_box_test, translate_rotate)
{
    box->translate(10, 0, 0);
    box->rotate_z(90, true);

    // max
    EXPECT_NEAR(box->get_max().x(), 20, EPSILON);
    EXPECT_NEAR(box->get_max().y(), 10, EPSILON);
    EXPECT_NEAR(box->get_max().z(), 10, EPSILON);
    // min
    EXPECT_NEAR(box->get_min().x(), 10, EPSILON);
    EXPECT_NEAR(box->get_min().y(), 0, EPSILON);
    EXPECT_NEAR(box->get_min().z(), 0, EPSILON);
    // center
    EXPECT_NEAR(box->get_center().x(), 15, EPSILON);
    EXPECT_NEAR(box->get_center().y(), 5, EPSILON);
    EXPECT_NEAR(box->get_center().z(), 5, EPSILON);
}

TEST_F(obj_box_test, scale)
{
   box->scale(2);
    EXPECT_EQ(box->get_max(), Point3(15, 15, 15));
    EXPECT_EQ(box->get_min(), Point3(-5, -5, -5));
    EXPECT_EQ(box->get_center(), Point3(5, 5, 5));
}

TEST_F(obj_box_test, translate_scale)
{
    box->translate(10, 0, 0);
    box->scale(2);
    EXPECT_EQ(box->get_max(), Point3(25, 15, 15));
    EXPECT_EQ(box->get_min(), Point3(5, -5, -5));
    EXPECT_EQ(box->get_center(), Point3(15, 5, 5));
}
