#include <array>
#include <cmath>
#include "STL/stl_object/Point3.hpp"
#include "gtest/gtest.h"

static const constexpr float EPSILON = 10E-6;

class point3_test : public ::testing::Test
{
protected:
    Point3 p0;
    Point3 p1;
    Point3 p2;

    void SetUp() override
    {
        p0 = Point3{};
        p1 = Point3{1, 0, 1};
        p2 = Point3{std::array<float, 3>{0, -1, 3}};
    }
};

TEST_F(point3_test, init)
{
    // p0
    EXPECT_EQ(p0.x(), 0);
    EXPECT_EQ(p0.y(), 0);
    EXPECT_EQ(p0.z(), 0);
    // p1
    EXPECT_EQ(p1.x(), 1);
    EXPECT_EQ(p1.y(), 0);
    EXPECT_EQ(p1.z(), 1);
    // p2
    EXPECT_EQ(p2.x(), 0);
    EXPECT_EQ(p2.y(), -1);
    EXPECT_EQ(p2.z(), 3);

}

TEST_F(point3_test, translate)
{
    Transform t1 = Transform::Identity(); // a ne pas oublier Identity!
    t1.translate(Vect3{1, 0, 0});
    p0.transform(t1);
    EXPECT_EQ(p0.x(), 1);
    EXPECT_EQ(p0.y(), 0);
    EXPECT_EQ(p0.z(), 0);

    Transform t2 = Transform::Identity();
    t2.translate(Vect3{1, 2, -4});
    p2.transform(t2);
    EXPECT_EQ(p2.x(), 1);
    EXPECT_EQ(p2.y(), 1);
    EXPECT_EQ(p2.z(), -1);
}

TEST_F(point3_test, rotate)
{
    Transform r = Transform::Identity(); // a ne pas oublier Identity!
    r.rotate(AngleAxis(M_PI, Vect3{0, 0, 1}));
    p0.transform(r);
    EXPECT_EQ(p0.x(), 0);
    EXPECT_EQ(p0.y(), 0);
    EXPECT_EQ(p0.z(), 0);

    p1.transform(r);
    ASSERT_NEAR(p1.x(), -1, EPSILON);
    ASSERT_NEAR(p1.y(), 0, EPSILON);
    ASSERT_NEAR(p1.z(), 1, EPSILON);

    p2.transform(r);
    ASSERT_NEAR(p2.x(), 0, EPSILON);
    ASSERT_NEAR(p2.y(), 1, EPSILON);
    ASSERT_NEAR(p2.z(), 3, EPSILON);
}

TEST_F(point3_test, scale)
{
    Transform r = Transform::Identity(); // a ne pas oublier Identity!
    r.scale(2);
    p0.transform(r);
    EXPECT_EQ(p0.x(), 0);
    EXPECT_EQ(p0.y(), 0);
    EXPECT_EQ(p0.z(), 0);

    p1.transform(r);
    EXPECT_EQ(p1.x(), 2);
    EXPECT_EQ(p1.y(), 0);
    EXPECT_EQ(p1.z(), 2);

    p2.transform(r);
    EXPECT_EQ(p2.x(), 0);
    EXPECT_EQ(p2.y(), -2);
    EXPECT_EQ(p2.z(), 6);
}

