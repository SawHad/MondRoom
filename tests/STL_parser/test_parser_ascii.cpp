#include <string>
#include "gtest/gtest.h"
#include "STL/Parser/ParserAscii.hpp"
#include "STL/Parser/Parser.hpp"

static stl::ParserAscii parser;

const std::string path_bank = "../../bank_stl/";
static const std::string path_deco = path_bank + "Decorations/";
static const std::string path_meuble = path_bank + "Meuble/";
TEST(parser_ascii, bank_test)
{
    EXPECT_NE(parser(path_deco + "tetra_ascii.stl"), nullptr);
    EXPECT_NE(parser(path_deco + "cube.stl"), nullptr);
    EXPECT_NE(parser(path_meuble + "chaise.stl"), nullptr);

    //binary
    EXPECT_EQ(parser(path_deco + "tetra_bin.stl"), nullptr);
}

TEST(parser_ascii, file_not_exit)
{
    EXPECT_THROW(parser("no_exist.stl"), std::runtime_error);
}

TEST(parser_ascii, error)
{
    std::string test_file = "../../tests/STL_parser/test_files/";
    EXPECT_EQ(parser(test_file + "err_2_vertex.stl"), nullptr);
    EXPECT_EQ(parser(test_file + "err_different_name.stl"), nullptr);
    EXPECT_EQ(parser(test_file + "err_facet_not_end.stl"), nullptr);
    EXPECT_EQ(parser(test_file + "err_normal.stl"), nullptr);
    EXPECT_EQ(parser(test_file + "err_number.stl"), nullptr);
}


TEST(parser_ascii, auto_parse)
{
    EXPECT_NE(stl::Parser::parse(path_deco + "tetra_ascii.stl"), nullptr);
}

TEST(parser_ascii, test_outpout)
{
    std::string res = "solid tetraedre\n"
                      "    facet normal 0 0 -1\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 10 0 0\n"
                      "        vertex 0 10 0\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal -1 0 0\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 0 10 0\n"
                      "        vertex 0 0 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal 0 -1 0\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 10 0 0\n"
                      "        vertex 0 0 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal 0.57735 0.57735 0.57735\n"
                      "        outer loop\n"
                      "        vertex 0 10 0\n"
                      "        vertex 10 0 0\n"
                      "        vertex 0 0 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "endsolid tetraedre";

    auto obj = parser(path_deco + "tetra_ascii.stl");
    std::stringstream buffer;
    buffer << *obj;
    EXPECT_EQ(buffer.str(), res);
}