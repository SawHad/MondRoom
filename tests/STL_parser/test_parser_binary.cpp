#include <string>
#include "gtest/gtest.h"
#include "STL/Parser/ParserBinary.hpp"

static constexpr stl::ParserBinary parser;
static const std::string path_deco =  "../../bank_stl/Decorations/";

TEST(parser_binary, bank_test)
{
    //binary
    EXPECT_NE(parser(path_deco + "tetra_bin.stl"), nullptr);
}

TEST(parser_binary, file_not_exit)
{
    EXPECT_THROW(parser("no_exist.stl"), std::runtime_error);
}

TEST(parser_binary, file_is_ascii)
{
    //binary
    EXPECT_THROW(parser(path_deco + "tetra_ascii.stl"),
                 stl::binary::ExceptionAscii);
}

TEST(parser_binary, auto_parse)
{
    EXPECT_NE(stl::Parser::parse(path_deco + "tetra_bin.stl"), nullptr);
}

TEST(parser_binary, test_outpout)
{
    std::string res = "solid tetraedre\n"
                      "    facet normal 0 0 -1\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 10 0 0\n"
                      "        vertex 0 10 0\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal -1 0 0\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 0 10 0\n"
                      "        vertex 0 0 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal 0 -1 0\n"
                      "        outer loop\n"
                      "        vertex 0 0 0\n"
                      "        vertex 10 0 0\n"
                      "        vertex 0 0 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "    facet normal 0.57735 0.57735 0.57735\n"
                      "        outer loop\n"
                      "        vertex 0 10 0\n"
                      "        vertex 10 0 0\n"
                      "        vertex 0 0 10\n"
                      "        endloop\n"
                      "    endfacet\n"
                      "\n"
                      "endsolid tetraedre";

    auto obj = parser(path_deco + "tetra_bin.stl");
    std::stringstream buffer;
    obj->set_name("tetraedre");
    buffer << *obj;
    EXPECT_EQ(buffer.str(), res);
}
