FROM gcc:latest

USER root

COPY mondRoom.sh install.sh

RUN apt-get update && \
    ./install.sh install \
