#!/bin/sh -e
ORIGIN=$(pwd)
BUILD="${ORIGIN}/build"
BIN="${BUILD}/src/mondRoom"
BINARY="${ORIGIN}/mondRoom"

install_lib() {
  echo "installation des library"
  apt-get install -y \
    cmake \
    g++ \
    flex \
    bison \
    libeigen3-dev \
    qtbase5-dev \
    libqt5opengl5-dev \
    fstl
}

install_gtest() {
  echo "installation de gtest"
  apt-get install -y googletest

  cd /usr/src/googletest &&
    cmake . &&
    cmake --build . --target install

  cd "${ORIGIN}"
}

build_project() {
  FLAGS="DCMAKE_BUILD_TYPE=Release"
  echo 'compilation du projet'
  mkdir -p "${BUILD}"
  cd "${BUILD}" &&
    cmake ${FLAGS} .. &&
    make
  echo "copy du binaire \"${BINARY}\""
  ln -sf "${BIN}" "${BINARY}"
  cd "${ORIGIN}"
}

clean() {
  echo "remove du dossier build: \"${BUILD}\""
  rm -rf "${BUILD}"

  echo "suppression du lien symbolique: \"${BINARY}\""
  rm "${BINARY}"
}

lance_test() {
  build_project
  cd "${BUILD}"
  cd ./tests && ./unit_test
  cd "${ORIGIN}"
}

usage() {
  echo "Usage:"

  echo "Argument d'installation"
  echo "./mondRoom.sh install_lib          fait une installation des libraries de base"
  echo "./mondRoom.sh install_test         fait une installation de gtest"
  echo "./mondRoom.sh install              fait une installation de gtest et des library"
  echo "attention d'utilisé sudo si nécéssaire"
  echo "Ces arguments fonctionnent que sur une architechture munie de 'apt-get'"

  echo ""

  echo "Argument de build: (attention ne fait pas l'installation)"
  echo "./mondRoom.sh build                lance le build du projet"
  echo "./mondRoom.sh clean                clear le projet"
  echo "./mondRoom.sh run                  build et lance le projet"
  echo "./mondRoom.sh test                 build les test et lance les test"

  echo "vous pouvez utilisé plusieur argument en meme temps"
  exit 1
}

parse_arg() {
  case $1 in
  "install_lib")
    install_lib
    ;;
  "install_test")
    install_gtest
    ;;
  "install")
    install_lib
    install_gtest
    ;;
  "clean")
  clean
  ;;
  "build")
    build_project
    ;;
  "run")
    build_project
    ${BINARY}
    ;;
  "test")
    lance_test
    ;;
  "help")
    usage
    ;;
  *)
    echo "'$1' n'éxite pas."
    usage
    ;;
  esac
}

if [ "$#" -eq 0 ]; then
  usage
fi

for i in "$@"; do
  parse_arg "$i"
done

exit 0
