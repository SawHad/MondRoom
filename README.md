
![plot](src/Front/Resources/Logo/MondRoomSmall.png)

# MondRoom 


MondRoom est une application vous permettant de modéliser, dans un style unique et artistique, une pièce de maison et de l'imprimer en 3D.

MondRoom est assez intuitif.

Lorsque vous etes dans l'espace de creation, vous pouvez inserer et redimensionner des objets a votre guise.

Une fois terminé, vous pouvez lancer l'impression 3D ou sauvegarder votre projet pour plus tard.

MondRoom permet de faire deux rendu possible, selon vos envies:
*  Realiste - Vous obtiendrez une maquette conforme a vos dispositions et choix d'objets
*  Artistique - Amusez vous a marcher sur les pas de [`Piet Mondrian`](https://fr.wikipedia.org/wiki/Piet_Mondrian) et son celebre [`tableau`](https://www.connaissancedesarts.com/arts-expositions/composition-en-rouge-jaune-bleu-et-noir-de-mondrian-focus-sur-un-chef-doeuvre-11142780/) et confectionnez des maquettes aux dimensions uniques.

Pour cela, il vous suffit de suivre les etapes d'installation decrite ci-dessous.

## Composition du dépôt

Le dépôt est composé comme suit:

- [`Document/`](/Document) un dossier où se trouve divers fichiers pour le suivis du projet.
- [`src/`](/src) le code source.
- [`tests/`](/tests) les tests du code.
- [`bank_stl/`](/bank_stl) la banque d'objets pour la partie "modélisation".

# Installation et test

## Installation

### Dependances a voir

- [Cmake (version 3.13)](https://cmake.org/)
- [eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page)
- [flex et bison](https://www.gnu.org/software/bison/)
- [Gtest](https://github.com/google/googletest/blob/master/googletest/README.md)
- [qt 5](https://www.qt.io/)
- [fstl](https://github.com/fstl-app/fstl)

Vous pouvez, si vous etes sur une archtechture Debian, lancer:

```shell
sudo ./mondRoom.sh install
```

### Compilation

Pour installer le projet veuiller lancer le script shell suivant et suivre les instructions d'usage:
`./install.sh`

ou faire:

```shell script
mkdir build
cd build
cmake ..
make
```

## Test

Pour lancer les tests relatifs a notre projet, verifiez que la librairie *Gtest* est bien installé sur votre ordinateur et que vous avez bien compilé le projet (`./install.sh install_test`).

Pour lancer les tests, tapez: 
```shell script
`./mondRoom.sh test
```

Ou, se rendre dans votre dossier `build/` et taper:

```shell script
cd build
ctest
```

## Auteurs

* **Jérémy DAMOUR**
* **Sawssen HADDED**

