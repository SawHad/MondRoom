# Format stl
## stl ASCII
### simple exemple
```stl
solid name
facet normal ni nj nk
    outer loop
        vertex v1x v1y v1z
        vertex v2x v2y v2z
        vertex v3x v3y v3z
    endloop
end facet
endsolid name
```

### grammaire
```bnf
<object> ::= "solid" [ name ] { facet } "endsolid" [ name ]

<name> ::= [::alpha::]+

<facet> ::= "facet" "normal" <point3> <loop> "endfacet"

<loop> ::= "outer" "loop" <vertex3> "endloop"

<vertex3> ::= <vertex> <vertex> <vertex>

<vertex> ::= "vertex" <point3>

<point3> ::= <floatnumber> <floatnumber> <floatnumber>
```

## stl Binary
```
UINT8[80] – en-tête
UINT32 – Nombre de triangles

foreach triangle
REAL32[3] – Vecteur normal
REAL32[3] – Sommet 1
REAL32[3] – Sommet 2
REAL32[3] – Sommet 3
UINT16 – Mot de contrôle
end
```
Les 80 premiers octets sont un commentaire.
On a ensuite un entier sur 32 bit non signé renprésentant le nombre de triangle
En suite nous avons les triangles avec:
leur normal (3 float), puis leur 3 sommet (avec 3 float)

## lien utile
[Wikipedia](https://fr.wikipedia.org/wiki/Fichier_de_stéréolithographie) \
[Document explicatif](https://www.lesimpressions3d.com/stl-file-format-3d-printing-simply-explained/) \
[Convertisseur de stl](https://www.meshconvert.com/) \
[stl viewer online](https://www.viewstl.com/)
