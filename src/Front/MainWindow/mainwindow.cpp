#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QFileDialog>

MainWindow::MainWindow(QWidget* parent)
        : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Menu");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_AboutButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::on_QuitButton_clicked()
{
    MainWindow::close();
}

void MainWindow::on_HelpButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_BackHomeButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_BackHomeButton2_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_NewProjetButton_clicked()
{ 
    form* fo = new form();
    fo->show();
    this->close();
}

void MainWindow::on_ImportBot_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this);
    
    if(path.isEmpty())
        return;

    creationwindow* cr = creationwindow::import_project(
            path.toStdString() + "/");
    cr->show();
    this->close();
}
