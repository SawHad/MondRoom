#include "form.hpp"
#include "ui_form.h"

form::form(QWidget* parent)
        : QDialog(parent), ui(new Ui::form)
{
    ui->setupUi(this);
}

form::~form()
{
    delete ui;
}

void form::on_Annuler_clicked()
{
    MainWindow* m = new MainWindow();
    m->show();
    this->close();
}

void form::on_Defaut_clicked()
{
    creationwindow* cr = new creationwindow();
    cr->show();
    this->close();
}

void form::on_Continuer_clicked()
{
    creationwindow* cr = new creationwindow(nullptr,
                                            ui->height->value(),
                                            ui->width->value());
    cr->show();
    this->close();
}
