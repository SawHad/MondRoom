#ifndef FORM_HPP
#define FORM_HPP

#include <QDialog>
#include "../MainWindow/mainwindow.hpp"
#include "../CreationWindow/creationwindow.hpp"

namespace Ui {
class form;
}

class form : public QDialog
{
    Q_OBJECT

public:
    explicit form(QWidget *parent = nullptr);
    ~form();

private slots:
    void on_Annuler_clicked();
    void on_Defaut_clicked();
    void on_Continuer_clicked();

private:
    Ui::form *ui;
};

#endif // FORM_HPP
