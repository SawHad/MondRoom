#include "actionRect.hpp"

static void invert(float& val, const bool& inverted)
{
    if (inverted)
        val = -val;
}

std::function<void(RectStl&)> get_func_translate_x(float tx,
                                                   const bool& inverted)
{
    invert(tx, inverted);
    return [&tx](RectStl & rect){ rect.translate_x(tx); };
}

std::function<void(RectStl&)> get_func_translate_y(float ty,
                                                   const bool& inverted)
{
    invert(ty, inverted);
    return [&ty](RectStl & rect){ rect.translate_y(ty); };
}

std::function<void(RectStl&)> get_func_rotate(float angle,
                                              const bool& inverted)
{
    invert(angle, inverted);
    return [&angle](RectStl & rect){ rect.rotate(angle); };
}

std::function<void(RectStl&)> get_func_scale(float factor,
                                             const bool& inverted)
{
    invert(factor, inverted);
    return [&factor](RectStl & rect){ rect.scale(factor); };
}

std::function<void(RectStl&)> get_func_set_color(QColor c)
{
    return [&c](RectStl & rect){ rect.set_color(c); };
}
