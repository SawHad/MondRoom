#include "Scene.hpp"
#include "../../STL/stl_object/ObjectBox.hpp"
#include <QGraphicsRectItem>

Scene::Scene(QObject* parent, double height, double width)
        : QGraphicsScene{parent},
          height_grid{space_grid * height / 3},
          width_grid{space_grid * width / 3},
          height_scene{height},
          width_scene{width}

{
    draw_grid();
}

std::vector<RectStl*> Scene::list_rect(bool selected)
{
    std::vector<RectStl*> list;
    for (QGraphicsItem* item: items())
    {
        if (item->type() == RectStl::Type)
        {
            RectStl* rect_stl = dynamic_cast<RectStl*>(item);
            if (not selected or rect_stl->isSelected())
                list.push_back(rect_stl);
        }
    }
    return list;
}

void Scene::apply_on_rect_active(std::function<void(RectStl&)> func)
{
    for (RectStl* rect: list_rect(true))
        func(*rect);
}

void Scene::remove_rect()
{
    std::vector<RectStl*> list = list_rect(true);
    for (unsigned i = 0; i < list.size(); i++)
        delete list.at(i);

}

void Scene::draw_grid()
{
    QPen pen = QPen(Qt::black);
    pen.setWidth(3);

    // vertical line
    for (int x = -width_grid; x <= width_grid; x += space_grid)
        addLine(x, -height_grid, x, height_grid, pen);

    //horizontal line
    for (int y = -height_grid; y <= height_grid; y += space_grid)
        addLine(-width_grid, y, width_grid, y, pen);
}


std::shared_ptr<ObjectStl> Scene::getFinalObjectStl(const std::string& name)
{
    const ObjectBox box_ground = ObjectBox(getGround(), true);
    std::vector<Triangle> all_triangle{box_ground.get_stl_object().get_triangle()};
    std::vector<RectStl*> all_rect = list_rect(false);
    auto func = [&all_triangle](RectStl* rect_stl)
    {
        const ObjectStl stl = rect_stl->get_stl_object();
        std::vector<Triangle> triangles_stl = stl.get_triangle();
        all_triangle.insert(all_triangle.end(),
                            triangles_stl.begin(),
                            triangles_stl.end());
    };

    std::for_each(all_rect.begin(), all_rect.end(), func);

    ObjectBox object_box_final = ObjectStl(name, all_triangle).to_object_box();
    object_box_final.scale( (width_scene * 10 ) / (width_grid * 2));
    return std::make_shared<ObjectStl>(object_box_final.get_stl_object());
}

GroundObject Scene::getGround() const
{
    return GroundObject(width_grid * 2, height_grid * 2);
}
