#pragma once
#include <QGraphicsScene>
#include <functional>
#include <memory>
#include "../../STL/stl_object/GroundObject.hpp"
#include "RectStl.hpp"

// 1 -> 3cm
// 0.5 -> 1.5cm

class Scene:  public QGraphicsScene
{
public:
    inline static const qreal space_grid = 70;
private:
    qreal height_grid; //hauteur  minimum 1 (3cm) maximum 4 (12cm)
    qreal width_grid; //largeur minimum 1 (3cm) maximum 6 (18cm)
    double height_scene;
    double width_scene;
private:
    /// draw grid in scene
    void draw_grid();

public:
    /// ctor
    /// @param parent : parent widget
    explicit Scene(QObject* parent, double height = 2, double width = 3);

    /// dtor
    ~Scene() = default;

    /// list ob rect
    /// @param seleted Filter selected rectstl
    /// @return list of rect
    std::vector<RectStl*> list_rect(bool seleted=true);

    /// apply func on rectStl active in scene
    /// @param func function to apply
    void apply_on_rect_active(std::function<void(RectStl&)> func);

    /// remove item selected
    void remove_rect();

    /// make an object stl with all stl object in scene
    /// @return Object stl
    std::shared_ptr<ObjectStl> getFinalObjectStl(const std::string& name ="");

    /// return ground object
    /// @return
    GroundObject getGround() const;
};


