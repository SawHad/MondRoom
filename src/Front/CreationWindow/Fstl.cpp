#include <fstream>
#include "Fstl.hpp"
const char* Fstl::path_stl_tmp{"/tmp/tempStlFile3d.stl"};

Fstl::Fstl(const ObjectStl& object_stl)
    :object_stl{object_stl}
{}

Fstl::~Fstl()
{
    std::remove(path_stl_tmp);
}

void Fstl::operator()()
{
    object_stl.to_file(path_stl_tmp, false);
    run_fstl_bin(path_stl_tmp);
}

void Fstl::run_fstl_bin(const std::string& path_stl)
{
    system(("fstl " + path_stl).c_str());
}

