#include "RectStl.hpp"
#include <QBrush>
#include <QPainter>
#include <QGraphicsSceneEvent>
#include <QCursor>
#include <QDebug>
#include "Scene.hpp"

int RectStl::counter = 0;

RectStl::RectStl(const ObjectStl& stl_object, const bool& middle)
        : QGraphicsRectItem{}, stl_box{stl_object, true}, id{RectStl::counter}
{
    QRectF rect{get_2d(stl_box.get_min()),
                get_2d(stl_box.get_max())};
    setRect(rect);

    setTransformOriginPoint(this->rect().center());
    setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
    set_color(Qt::red);
    RectStl::counter++;

    if (!middle)
    {
        const ObjectBox box_no_center{stl_object};
        translate_x(box_no_center.get_center().x());
        translate_y(box_no_center.get_center().z());
    }

    // add object name ???
}

RectStl::~RectStl()
{}

QPoint RectStl::get_2d(const Point3& point)
{
    return QPoint(point.x(), point.y());
}

void RectStl::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    QGraphicsRectItem::mouseReleaseEvent(event);
}

void RectStl::translate_x(float tx)
{
    setX(x() + tx);
}

void RectStl::translate_y(float ty)
{
    setY(y() + ty);
}

void RectStl::rotate(float angle)
{
    setRotation(rotation() + angle);
}

void RectStl::scale(float factor)
{
    setScale(QGraphicsRectItem::scale() + factor);
}

void RectStl::set_color(QColor c)
{
    setPen(QPen(c));
    setBrush(QBrush(c));
}

ObjectStl RectStl::get_stl_object() const
{
    ObjectBox ret_box{stl_box};
    // rotation
    ret_box.rotate_z(QGraphicsRectItem::rotation(), true);

    // scale
    ret_box.scale(QGraphicsRectItem::scale());

    // translation
    Vect3 cur_pos = ret_box.get_center();
    Vect3 new_pos{(float) x(),
                  (float) y(),
                  ret_box.get_max().z()};
    ret_box.translate(new_pos - cur_pos);


    return ret_box.get_stl_object();
}

int RectStl::type() const
{
    return Type;
}

int RectStl::get_id() const
{
    return id;
}

void RectStl::to_file(std::string path_dir) const
{
    const ObjectStl stl_obj = get_stl_object();
    const std::string path = path_dir
                             + std::to_string(get_id())
                             + "-" + stl_obj.get_name() + ".stl";
    stl_obj.to_file(path);
}
