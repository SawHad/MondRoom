#pragma once
#include "../../STL/stl_object/ObjectStl.hpp"

class Fstl
{
private:
    const ObjectStl& object_stl;


public:
    // path tmp file
    static const char* path_stl_tmp;
    /// ctor
    Fstl(const ObjectStl& object_stl);

    /// dtor
    ~Fstl();

    /// operator()
    void operator()();


    /// to run fstl on stlpath
    /// @param path path of stl file
    static void run_fstl_bin(const std::string& path);
};