#pragma once
#include <QMainWindow>
#include <QPushButton>
#include <QWidget>
#include <QGraphicsView>
#include <QDir>
#include <vector>
#include "../MainWindow/mainwindow.hpp"
#include "RectStl.hpp"
#include "Scene.hpp"

namespace Ui
{
    class creationwindow;
}

class creationwindow : public QMainWindow
{
Q_OBJECT

public:
    inline static float translation_num = 5;
    inline static float scale_num = 1.5;
    inline static const std::string setting_file_name = "setting.txt";

private:
    Ui::creationwindow* ui;
    Scene* scene;
    std::string path_project = "";
    double height;
    double widht;

    bool invert_action = false;
    void add_objet(const std::string& path, bool middle = true);
    void save_project();
    void create_setting_file();

    static creationwindow* load_settings(const std::string& path_dir);

public:

    explicit creationwindow(QWidget* parent = nullptr,
                            double height = 6,
                            double width = 9);

    static creationwindow* import_project(const std::string& path_dir);
    ~creationwindow();

private slots:
    void on_actionRetour_Menu_triggered();
    void on_actionQuitter_triggered();
    void on_actionAide_triggered();

    // stl object action
    void on_actionInserer_un_objet_triggered();
    void on_actionSupprimer_un_objet_triggered();
    void on_actionAvancer_triggered();
    void on_actionReculer_triggered();
    void on_actionMonter_triggered();
    void on_actionDescendre_triggered();
    void on_actionRotation_90_droite_triggered();
    void on_actionRotation_90_gauche_triggered();
    void on_actionAgrandir_triggered();
    void on_actionRetrecir_triggered();
    void on_pushBlack_clicked();
    void on_pushBlue_clicked();
    void on_pushRed_clicked();
    void on_pushYellow_clicked();
    void on_pushButtonVisualise_clicked();

    // action keys
    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);
    void on_actionEnregistrer_sous_triggered();
    void on_actionEnregistrer_triggered();
    void on_actionOuvrir_un_projet_triggered();
    void on_actionNouveau_projet_triggered();
    void on_actionImprimer_triggered();

};
