#include "creationwindow.hpp"
#include <qfiledialog.h>
#include <QKeyEvent>
#include <QMessageBox>
#include <fstream>
#include <filesystem>
#include "ui_creationwindow.h"
#include "../../STL/Parser/Parser.hpp"
#include "RectStl.hpp"
#include "actionRect.hpp"
#include "Fstl.hpp"

creationwindow::creationwindow(QWidget* parent,
                               double height, double width)
        : QMainWindow{parent},
        ui{new Ui::creationwindow},
        height{height},
        widht{width}
{
    Q_INIT_RESOURCE(Resources);
    setWindowTitle("Modelisation");
    ui->setupUi(this);

    // graphic view and scene setup
    scene = new Scene(ui->m_graphicsView, height, width);

    ui->m_graphicsView->setScene(scene);
    ui->m_graphicsView->setRenderHint(QPainter::Antialiasing);
    ui->m_graphicsView->setCacheMode(QGraphicsView::CacheBackground);
    ui->m_graphicsView->setViewportUpdateMode(
            QGraphicsView::BoundingRectViewportUpdate);
    ui->m_graphicsView->setAlignment(Qt::AlignCenter);
}

creationwindow::~creationwindow()
{
    Q_CLEANUP_RESOURCE(Resources);
    delete ui;
    delete scene;
}

void creationwindow::on_actionQuitter_triggered()
{
    this->close();
}

void creationwindow::on_actionRetour_Menu_triggered()
{
    MainWindow* mw = new MainWindow;

    this->close();
    mw->show();
}

void creationwindow::on_actionInserer_un_objet_triggered()
{
    try
    {
        QString path_obj = QFileDialog::getOpenFileName(this,
                                                        "Ouvrir un fichier",
                                                        "bank_stl",
                                                        "Fichier (*.stl)");
        if (path_obj.isEmpty())
            return;
        add_objet(path_obj.toStdString());
    }
    catch (stl::ExceptionStlFile& err)
    {
        QMessageBox messageBox;
        messageBox.critical(this, "Erreur fichier stl", err.what());
        messageBox.setFixedSize(500, 200);
        on_actionRetour_Menu_triggered();
    }
}

void creationwindow::on_actionSupprimer_un_objet_triggered()
{
    scene->remove_rect();
}

void creationwindow::add_objet(const std::string& path, bool middle)
{
    std::shared_ptr<ObjectStl> stl_obj = stl::Parser::parse(path);
    scene->addItem(new RectStl(*stl_obj, middle));
}

void creationwindow::on_actionAvancer_triggered()
{
    scene->apply_on_rect_active(get_func_translate_x(translation_num, false));
}

void creationwindow::on_actionReculer_triggered()
{
    scene->apply_on_rect_active(get_func_translate_x(translation_num, true));
}

void creationwindow::on_actionMonter_triggered()
{
    scene->apply_on_rect_active(get_func_translate_y(translation_num, true));
}

void creationwindow::on_actionDescendre_triggered()
{
    scene->apply_on_rect_active(get_func_translate_y(translation_num, false));
}


static const float RIGHT_ANGLE = 90;

void creationwindow::on_actionRotation_90_droite_triggered()
{
    scene->apply_on_rect_active(get_func_rotate(RIGHT_ANGLE, invert_action));
}

void creationwindow::on_actionRotation_90_gauche_triggered()
{
    invert_action = true;
    on_actionRotation_90_droite_triggered();
    invert_action = false;
}

void creationwindow::on_actionAgrandir_triggered()
{
    scene->apply_on_rect_active(get_func_scale(scale_num, invert_action));
}

void creationwindow::on_actionRetrecir_triggered()
{
    scene->apply_on_rect_active(get_func_scale(scale_num, !invert_action));
}

void creationwindow::on_pushBlack_clicked()
{
    scene->apply_on_rect_active(get_func_set_color(Qt::black));
}

void creationwindow::on_pushBlue_clicked()
{
    scene->apply_on_rect_active(get_func_set_color(Qt::blue));
}

void creationwindow::on_pushRed_clicked()
{
    scene->apply_on_rect_active(get_func_set_color(Qt::red));
}

void creationwindow::on_pushYellow_clicked()
{
    scene->apply_on_rect_active(get_func_set_color(Qt::yellow));
}

void creationwindow::keyPressEvent(QKeyEvent* event)
{
    switch (event->key())
    {
        case Qt::Key::Key_Shift:
            invert_action = true;
            break;
        default:
            break;
    }
    QWidget::keyPressEvent(event);
}

void creationwindow::keyReleaseEvent(QKeyEvent* event)
{
    switch (event->key())
    {
        case Qt::Key::Key_Shift:
            invert_action = false;
            break;

        default:
            break;
    }
    QWidget::keyReleaseEvent(event);
}

static const char* help_string =
        "\nCeci est l'espace de creation de votre pièce."
        "\n\nVous pouvez importer des objets et les deplacer/redimentionner comme bon vous semble via la barre d'outil."
        "\nUne fois terminé, vous pouvez imprimer votre travail via une imprimante 3D et/ou le sauvegarder pour le modifier plus tard."
        "\n\nPour connaitre la fonctionnalité de chaque outils, glissez votre souris dessus."
        "\n\nN'oubliez pas de sauvegarder votre travail !";

void creationwindow::on_actionAide_triggered()
{
    QMessageBox::information(this, "Aide", help_string);
}

creationwindow* creationwindow::load_settings(const std::string& path_dir)
{
    double height, widht;
    std::ifstream setting_file{path_dir + creationwindow::setting_file_name};
    setting_file >> height >> widht;
    setting_file.close();

    creationwindow* cr = new creationwindow(nullptr, height, widht);
    cr->path_project = path_dir;

    return cr;

}

creationwindow* creationwindow::import_project(const std::string& path_dir)
{
    // load settings
    creationwindow* project = load_settings(path_dir);

    QDir dir(path_dir.c_str());
    dir.setFilter(QDir::Files);
    QFileInfoList list = dir.entryInfoList();
    auto func = [&project](const QFileInfo& f_info)
    {
        project->add_objet(f_info.filePath().toStdString(), false);
    };
    std::for_each(list.begin(), list.end(), func);
    return project;
}

static void deleteDirectoryContents(const std::string& dir_path)
{
    for (const auto& entry : std::filesystem::directory_iterator(dir_path))
        std::filesystem::remove_all(entry.path());
}

void creationwindow::create_setting_file()
{
    std::ofstream setting_file{path_project + setting_file_name};
    setting_file << height << " " << widht;
    setting_file.close();
}

void creationwindow::save_project()
{
    deleteDirectoryContents(path_project);

    create_setting_file();
    std::vector<RectStl*> list_rect = scene->list_rect(false);

    auto func = [&path_dir=this->path_project](const RectStl* rect)
    { rect->to_file(path_dir);};
    std::for_each(list_rect.begin(), list_rect.end(), func);

    QMessageBox::information(this, "Enregistrement",
                             "Votre projet à été enregistré avec succès !");
}

void creationwindow::on_actionEnregistrer_sous_triggered()
{
    try
    {
        QString path = QFileDialog::getExistingDirectory(this);

        if (path.isEmpty())
            return;
        path_project = (path + "/").toStdString();
        save_project();
    }
    catch (stl::ExceptionStlFile& err)
    {
        QMessageBox messageBox;
        messageBox.critical(this, "Error file", err.what());
        messageBox.setFixedSize(500, 200);
        on_actionRetour_Menu_triggered();
    }
}

void creationwindow::on_actionEnregistrer_triggered()
{
    if (path_project.empty())
        on_actionEnregistrer_sous_triggered();
    else
        save_project();
}

void creationwindow::on_actionOuvrir_un_projet_triggered()
{
    std::string path = QFileDialog::getExistingDirectory(this).toStdString();
    creationwindow* cr = import_project(path + "/");
    cr->show();
    this->close();
}

void creationwindow::on_actionNouveau_projet_triggered()
{
    form* f = new form();
    f->show();
    this->close();
}

void creationwindow::on_actionImprimer_triggered()
{
    try
    {
        std::string path_save = QFileDialog::getSaveFileName(
                this,
                "Enregistrer "
                "sous",
                QString(),
                "Fichier (*.stl)").toStdString();

        if (path_save.empty())
            return;

        // get name
        std::string base_filename = path_save.substr(
                path_save.find_last_of("/\\") + 1);
        std::string::size_type const p(base_filename.find_last_of('.'));
        std::string name = base_filename.substr(0, p);

        std::shared_ptr<ObjectStl> final_object = scene->getFinalObjectStl(name);
        final_object->to_file(path_save);

        QMessageBox::information(this, "Export",
                                 "Votre projet à été exporté avec succès !"
                                 "\nVous pouvez l'imprimer.");

    }
    catch (stl::ExceptionStlFile& err)
    {
        QMessageBox messageBox;
        messageBox.critical(this, "Error file", err.what());
        messageBox.setFixedSize(500, 200);
    }
}

void creationwindow::on_pushButtonVisualise_clicked()
{
    std::shared_ptr<ObjectStl> final_object = scene->getFinalObjectStl();
    Fstl fstl{*final_object};
    fstl();
}
