#include "ObjectBox.hpp"
#include "ObjectStl.hpp"

ObjectBox::ObjectBox(const ObjectStl& stl, bool center)
        : objectstl{stl}
{
    reset_box();
    if (center)
        translate(-Vect3(get_center()));
}

ObjectBox::~ObjectBox()
{}

void ObjectBox::reset_box()
{
    box.setEmpty();
    for (const PointEigen& point : objectstl.get_points_eigen())
        box.extend(point);
}

Point3 ObjectBox::get_max() const
{
    return Point3(box.max());
}

Point3 ObjectBox::get_min() const
{
    return Point3(box.min());
}

Point3 ObjectBox::get_center() const
{
    return Point3(box.center());
}

float ObjectBox::get_volume() const
{
    return box.volume();
}

const ObjectStl& ObjectBox::get_stl_object() const
{
    return objectstl;
}


static float to_rad(float deg)
{
    return M_PI * deg / 180;
}


ObjectBox& ObjectBox::rotate_x(float angle, bool deg)
{
    return rotate(Vect3::UnitX(), angle, deg);
}

ObjectBox& ObjectBox::rotate_y(float angle, bool deg)
{
    return rotate(Vect3::UnitY(), angle, deg);
}

ObjectBox& ObjectBox::rotate_z(float angle, bool deg)
{
    return rotate(Vect3::UnitZ(), angle, deg);
}


bool is_angle_90(float angle)
{
    int angle_deg = 180 * angle / M_PI;
    return angle_deg % 90 == 0;
}

ObjectBox& ObjectBox::rotate(const Vect3& axe, float angle, bool deg)
{
    // change to rad if deg
    angle = deg ? to_rad(angle) : angle;

    // create mat
    Transform mat_transfo = Transform::Identity();
    mat_transfo.rotate(AngleAxis(angle, axe));

    // return to center
    Vect3 vec_center = get_center();
    translate(-vec_center);

    // apply rotation
    objectstl.transform(mat_transfo);

    // re apply translate
    translate(vec_center);

    reset_box();

    return *this;
}

ObjectBox& ObjectBox::translate(float tx, float ty, float tz)
{
    return translate(Vect3(tx, ty, tz));
}

ObjectBox& ObjectBox::translate(const Vect3& direction)
{
    // create matrix translation
    Transform mat_transfo = Transform::Identity();
    mat_transfo.translate(direction);

    // apply matrix to object
    objectstl.transform(mat_transfo);

    // translate box
    box.translate(direction);

    return *this;
}

ObjectBox& ObjectBox::scale(float size)
{
    // set point min orig
    Vect3 point_center = get_center();
    translate(-point_center);

    // create matrix
    Transform mat_transfo = Transform::Identity();
    mat_transfo.scale(size);

    // apply matrix
    objectstl.transform(mat_transfo);

    // set origin to point_min
    translate(point_center);

    // reset box
    reset_box();

    return *this;
}