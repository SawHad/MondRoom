#pragma once
#include <ostream>
#include <array>
#include <eigen3/Eigen/Geometry>

using Vect3 = Eigen::Vector3f;
using Transform = Eigen::Transform<float, 3, Eigen::TransformTraits::Affine>;
using AngleAxis = Eigen::AngleAxis<float>;
using PointEigen = Vect3;

class Point3
{
protected:
    Vect3 vec;

public:
    Point3();
    Point3(float x, float y, float z);
    Point3(const std::array<float, 3>& f);
    Point3(const Vect3& vect);

    virtual ~Point3() = default;

    float x() const;
    float y() const;
    float z() const;

    virtual Point3& transform(const Transform& tranformation);

    operator PointEigen() const;
    bool operator==(const Point3& other) const;
    bool operator!=(const Point3& other) const;

    friend std::ostream& operator<<(std::ostream& stream,
                                    const Point3& point3);
};
