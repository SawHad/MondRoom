#include "Vect3Normal.hpp"

const Vect3Normal Vect3Normal::X{1, 0, 0};
const Vect3Normal Vect3Normal::Y{0, 1, 0};
const Vect3Normal Vect3Normal::Z{0, 0, 1};

Vect3Normal::Vect3Normal()
        : Point3{}
{}

Vect3Normal::Vect3Normal(float x, float y, float z)
        : Point3{x, y, z}
{
    vec = vec.normalized();
}

Vect3Normal::Vect3Normal(const std::array<float, 3>& tab)
        : Vect3Normal{tab[0], tab[1], tab[2]}
{}

Vect3Normal::Vect3Normal(const Vect3Normal& other)
        : Point3{other.x(), other.y(), other.z()}
{}

Vect3Normal& Vect3Normal::operator=(const Vect3Normal& other)
{
    vec = other.vec;
    return *this;
}

Vect3Normal& Vect3Normal::transform(const Transform& tranformation)
{
    Mat3 normal_matrix = tranformation.linear().inverse().transpose();
    vec = (normal_matrix * vec).normalized();
    return *this;
}

Vect3Normal Vect3Normal::operator-() const
{
    return Vect3Normal(-x(), -y(), -z());
}
