file(GLOB SOURCES LIST_DIRECTORIES true *.hpp *.cpp)

project(StlObject_lib)

find_package(Eigen3 3.3 REQUIRED NO_MODULE)

add_library(${PROJECT_NAME} ${SOURCES})

target_link_libraries(${PROJECT_NAME} Eigen3::Eigen)
