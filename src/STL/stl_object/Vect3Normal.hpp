#pragma once
#include "Point3.hpp"

class Vect3Normal : public Point3
{
    using Mat3 = Eigen::Matrix3f;
public:
    Vect3Normal();
    Vect3Normal(float x, float y, float z);
    Vect3Normal(const std::array<float, 3>& arr);
    Vect3Normal(const Vect3Normal& other);

    Vect3Normal& operator=(const Vect3Normal& other);

    virtual ~Vect3Normal() = default;

    Vect3Normal& transform(const Transform &tranformation) override;
    Vect3Normal operator-() const;

    static const Vect3Normal X;
    static const Vect3Normal Y;
    static const Vect3Normal Z;
};
