#include "ObjectStl.hpp"
#include "ObjectBox.hpp"

ObjectStl::ObjectStl(const std::string& name,
                     const std::vector<Triangle>& list_triangle)
        : name{name}, list_triangle{list_triangle}
{
}

ObjectStl::ObjectStl(const ObjectStl& other)
        : ObjectStl{other.name, other.list_triangle}
{
}

ObjectStl& ObjectStl::operator=(const ObjectStl& other)
{
    name = other.get_name();
    list_triangle = other.list_triangle;
    return *this;
}

std::string ObjectStl::get_name() const
{
    return "objet";
}

std::vector<Triangle> ObjectStl::get_triangle() const
{
    return list_triangle;
}

void ObjectStl::set_name(const std::string& new_name)
{
    name = new_name;
}

ObjectStl& ObjectStl::transform(const Transform& tranformation)
{
    for (Triangle& triangle: list_triangle)
        triangle.transform(tranformation);
    return *this;
}

std::vector<Point3> ObjectStl::get_points() const
{
    std::vector<Point3> res;
    for (const Triangle& triangle: list_triangle)
        for (Point3 point: triangle.get_points())
            res.push_back(point);
    return res;
}

static auto p_to_eigen = [](const Point3& point)
{ return (PointEigen) point; };

std::vector<PointEigen> ObjectStl::get_points_eigen() const
{
    std::vector<Point3> points = get_points();
    std::vector<PointEigen> res;
    std::transform(points.begin(), points.end(),
                   std::back_inserter(res), p_to_eigen);
    return res;
}


std::ostream& operator<<(std::ostream& stream, const ObjectStl& o)
{
    stream << "solid " << o.get_name() << std::endl;

    for (const Triangle& triangle: o.list_triangle)
        stream << triangle << std::endl << std::endl;

    stream << "endsolid " << o.get_name();
    return stream;
}

ObjectBox ObjectStl::to_object_box(bool center)
{
    return ObjectBox(*this, center);
}

ObjectStl::operator ObjectBox()
{
    return to_object_box(false);
}
