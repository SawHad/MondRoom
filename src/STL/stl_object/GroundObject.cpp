#include "GroundObject.hpp"

static std::array<Point3, 8> get_points(const double& wight,
                                        const double& length,
                                        const double& height)
{
    std::array<double, 2> wight_l{0, wight};
    std::array<double, 2> length_l{0, length};
    std::array<double, 2> height_l{0, -height};

    std::array<Point3, 8> points{};
    int i = 0;
    for (const float w : wight_l)
        for (const float l : length_l)
            for (const float h : height_l)
                points[i++] = Point3(w, l, h);
    return points;
}


static std::vector<Triangle> get_triangles(const double& wight,
                                           const double& length,
                                           const double& height)
{
    const auto& points = ::get_points(wight, length, height);
    return std::vector<Triangle>{
            {
                    Triangle(-Vect3Normal::X, points[0], points[4], points[5]),
                    Triangle(-Vect3Normal::X, points[0], points[1], points[5]),

                    Triangle(Vect3Normal::X, points[2], points[3], points[6]),
                    Triangle(Vect3Normal::X, points[3], points[6], points[7]),

                    Triangle(-Vect3Normal::Y, points[0], points[2], points[4]),
                    Triangle(-Vect3Normal::Y, points[2], points[4], points[6]),

                    Triangle(Vect3Normal::Y, points[1], points[3], points[5]),
                    Triangle(Vect3Normal::Y, points[3], points[5], points[7]),

                    Triangle(-Vect3Normal::Z, points[0], points[1], points[2]),
                    Triangle(-Vect3Normal::Z, points[1], points[2], points[3]),

                    Triangle(Vect3Normal::Z, points[4], points[5], points[6]),
                    Triangle(Vect3Normal::Z, points[5], points[6], points[7]),
            }
    };
}

GroundObject::GroundObject(const double& wight,
                           const double& length,
                           const double& height)
        : ObjectStl(NAME, get_triangles(wight, length, height))
{}
