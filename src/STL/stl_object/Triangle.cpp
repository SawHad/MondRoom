#include "Triangle.hpp"

#include <utility>

const std::string Triangle::indent = "    ";

Triangle::Triangle(const Vect3Normal& normal,
                   Triangle::arr_point3 points,
                   uint16_t color)
        : normal{normal}, points{std::move(points)}, color{color}
{}

Triangle::Triangle(const Vect3Normal& normal,
                   const Point3& p1, const Point3& p2, const Point3& p3,
                   uint16_t color)
        : Triangle{normal, arr_point3{p1, p2, p3}, color}
{}

Triangle& Triangle::transform(const Transform& tranformation)
{
    normal.transform(tranformation);

    for (Point3& point: points)
        point.transform(tranformation);
    return *this;
}

std::array<Point3, 3> Triangle::get_points() const
{
    return points;
}

Vect3Normal Triangle::get_normal() const
{
    return normal;
}

uint16_t Triangle::get_color() const
{
    return color;
}

const std::string indent1 = Triangle::indent;
const std::string indent2 = indent1 + indent1;

std::ostream& operator<<(std::ostream& stream, const Triangle& triangle)
{
    stream << indent1 << "facet normal " << triangle.normal << std::endl;

    stream << indent2 << "outer loop" << std::endl;

    for (const Point3& point: triangle.points)
        stream << indent2 << "vertex " << point << std::endl;

    stream << indent2 << "endloop" << std::endl;
    stream << indent1 << "endfacet";

    return stream;
}


