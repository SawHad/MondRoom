#pragma once
#include <iostream>
#include <cstdint>
#include "Point3.hpp"
#include "Vect3Normal.hpp"

class Triangle
{
public:
    using arr_point3 = std::array<Point3, 3>;
    static const std::string indent;
private:
    Vect3Normal normal;
    arr_point3 points;
    uint16_t color;

public:
    Triangle(const Vect3Normal& normal,
             arr_point3 points,
             uint16_t color = 0);
    Triangle(const Vect3Normal& normal,
             const Point3& p1, const Point3& p2, const Point3& p3,
             uint16_t color = 0);
    ~Triangle() = default;

    std::array<Point3, 3> get_points() const;

    Vect3Normal get_normal() const;

    uint16_t get_color() const;

    Triangle& transform(const Transform& tranformation);

    friend std::ostream& operator<<(std::ostream& stream,
                                    const Triangle& triangle);
};
