#pragma once 

#include "Triangle.hpp"
#include "Vect3Normal.hpp"

#include <string>
#include <cstdio>
#include <vector>
#include <ostream>

class ObjectBox;

class ObjectStl
{
private:
    // fields
    std::string name;

    /// methode use to create ascii stl file
    /// @param path: string path of file
    void to_ascii(const std::string& path) const;

    /// method use to create finary stl fil
    /// @param path: string path of file
    void to_binary(const std::string& path) const;

protected:
    // fields
    std::vector<Triangle> list_triangle;

public:
    // header size public
    static constexpr unsigned int HEADER_SIZE = 80;

    /// c-tor
    /// @param name: name of object
    /// @param list_triangle: vector<Triangle> list of Triangle
    ObjectStl(const std::string& name,
              const std::vector<Triangle>& list_triangle);

    /// copy c-tor
    /// @param other: object to make copy
    ObjectStl(const ObjectStl& other);

    /// d-tor
    ~ObjectStl() = default;

    /// operator =
    /// @param other: object to make copy
    /// @return this
    ObjectStl& operator=(const ObjectStl& other);

    /// getter list of all points
    /// @return vector<Point3>
    std::vector<Point3> get_points() const;

    /// getter list of all points
    /// @return vector<PointEigen>
    std::vector<PointEigen> get_points_eigen() const;

    /// getter name of object
    /// @return  string
    std::string get_name() const;

    /// getter list of all triangles
    /// @return vector<Triangle>
    std::vector<Triangle> get_triangle() const;

    /// setter name of object
    /// @param new_name: new name
    void set_name(const std::string& new_name);

    /// apply a tranformation matrinx on object
    /// @param tranformation: matrix tranformation
    /// @return *this
    ObjectStl& transform(const Transform& tranformation);

    /// make a file stl
    /// @param path: path of file to create
    /// @param ascii: boolean true = ascci; false = binary
    void to_file(std::string path, bool ascii = true) const;

    /// make an object box
    /// @return
    ObjectBox to_object_box(bool center = false);

    /// cast to object box
    /// @return
    operator ObjectBox();

    /// operator <<
    friend std::ostream& operator<<(std::ostream& stream,
                                    const ObjectStl& object_stl);
};