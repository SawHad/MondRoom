#pragma once
#include "ObjectStl.hpp"

class GroundObject: public ObjectStl
{
private:
    static inline const std::string NAME{"ground"};
public:
    GroundObject(const double& wight,
                 const double & length,
                 const double & height=1);
    ~GroundObject() = default;
};
