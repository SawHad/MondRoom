#include "ObjectStl.hpp"
#include <fstream>


static void write_header(std::ofstream& file, const std::string& name)
{
    // header
    const unsigned int end = name.length() < ObjectStl::HEADER_SIZE ?
                             name.length() : ObjectStl::HEADER_SIZE;
    char header[ObjectStl::HEADER_SIZE] = {};
    std::copy(name.begin(), name.begin() + end, header);

    file.write(header, ObjectStl::HEADER_SIZE);
}

static void write_num_triangle(std::ostream& file, const unsigned int num)
{
    uint32_t triangle_num = num;
    file.write((char*)&triangle_num, sizeof(uint32_t));
}

static void write_normal(std::ostream& file, const Vect3Normal& normal)
{
    float normal_w[3] = {normal.x(), normal.y(), normal.z()};
    file.write((char*)normal_w, sizeof(float) * 3);
}

static void write_point(std::ostream& file, const Point3& point)
{
    float point_w[3] = {point.x(), point.y(), point.z()};
    file.write((char*)point_w, sizeof(float) * 3);
}

static void write_control(std::ostream& file, const uint16_t& control_bit)
{
    uint16_t control_w = control_bit;
    file.write((char*)&control_w, sizeof(uint16_t));
}

static void write_triangles(std::ofstream& file,
                            const std::vector<Triangle>& list_triangle)
{
    // write triangle info
    for (const Triangle& triangle: list_triangle)
    {
        write_normal(file, triangle.get_normal());

        for (const Point3& point: triangle.get_points())
            write_point(file, point);

        write_control(file, triangle.get_color());
    }
}

static void test_file_correct(std::ofstream& file)
{
    if (!file)
    {
        file.close();
        throw std::runtime_error("Unable to create a file");
    }
}

void ObjectStl::to_binary(const std::string& path) const
{
    std::ofstream file(path, std::ios::binary);
    test_file_correct(file);

    // write data
    write_header(file, name);
    write_num_triangle(file, list_triangle.size());
    write_triangles(file, list_triangle);

    // end
    file.close();
}

void ObjectStl::to_ascii(const std::string& path) const
{
    std::ofstream file(path, std::ios::out | std::ios::trunc);
    test_file_correct(file);

    // write in file
    file << *this << std::endl;
    file.close();
}

void ObjectStl::to_file(const std::string path, bool ascii) const
{
    if (ascii)
        to_ascii(path);
    else
        to_binary(path);
}
