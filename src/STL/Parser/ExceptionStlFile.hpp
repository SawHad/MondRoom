#pragma once
#include <string>
#include <exception>

namespace stl
{
    class ExceptionStlFile : public std::exception
    {
    private:
        std::string message;

    public:
        /// ctor
        explicit ExceptionStlFile(const std::string& path);

        /// d-tor
        virtual ~ExceptionStlFile();

        /// what function
        /// @return
        const char* what() const noexcept override;
    };
}

