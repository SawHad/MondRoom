#include "Parser.hpp"
#include "ParserBinary.hpp"
#include "ParserAscii.hpp"
namespace stl
{
    std::shared_ptr<ObjectStl> Parser::parse(const std::string& path)
    {
        try
        {
            ParserBinary parser_binary;
            return parser_binary(path);
        }
        catch (const binary::ExceptionAscii&)
        {
            ParserAscii parser_ascii;
            auto res = parser_ascii(path);
            if (not res)
                throw ExceptionStlFile(path);
            return res;
        }
    }
}