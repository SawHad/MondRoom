#pragma once
namespace stl::binary
{
    template<typename T>
    void Parser::read(T* var, unsigned int size)
    {
        file->read((char*)var, size * sizeof(*var));
    }
}