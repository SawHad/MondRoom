#pragma once
#include <string>
#include <exception>

namespace stl::binary
{
    class ExceptionAscii : public std::exception
    {
        std::string message{"error ascii this file is an"};
    public:
        ExceptionAscii();
        virtual ~ExceptionAscii();
        const char* what() const noexcept override;
    };
}