include_directories(.)
include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(../../stl_object)

project(StlAsciiParser_lib)

find_package(BISON)
find_package(FLEX)

file(GLOB HH_FILES *.hpp)
file(GLOB SRC_FILES *.cpp)

bison_target(Parser parser.yy ${CMAKE_CURRENT_BINARY_DIR}/parser.cc)
flex_target(Scanner lexer.ll ${CMAKE_CURRENT_BINARY_DIR}/scanner.cc)

add_flex_bison_dependency(Scanner Parser)

add_library(${PROJECT_NAME}
        ${FLEX_Scanner_OUTPUTS}
        ${BISON_Parser_OUTPUTS}
        ${HH_FILES}
        ${SRC_FILES}
        )
target_link_libraries(${PROJECT_NAME} StlObject_lib)