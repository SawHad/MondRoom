%{     /* PARSER */
#include "parser.hh"
#include "Scanner.hpp"

#define yylex driver.scanner->yylex
%}

%code requires
{
  #include <iostream>
  #include <memory>
  #include "Driver.hpp"
  #include "location.hh"
  #include "position.hh"
}

%code provides
{
  namespace stl::ascii
  {
    // Forward declaration of the Driver class
    class Driver;

    inline void
    yyerror (const char* msg)
    {
      std::cerr << msg << std::endl;
    }
  }
}
%require "2.4"
%language "C++"
%locations
%defines
%debug
%define api.namespace {stl::ascii}
%define api.parser.class {Parser}
%parse-param {Driver &driver}
%lex-param {Driver &driver}
%define parse.error verbose

%code requires
{
//#include "../Object"
#include <array>
#include "../../stl_object/Point3.hpp"
#include "../../stl_object/Vect3Normal.hpp"
#include "../../stl_object/Triangle.hpp"
}

%union
{
    /* YYLTYPE */
    float number;
    std::string* string;
    std::array<float, 3>* coord_t;
    Point3* vertex_t;
    Triangle::arr_point3* vertex3_t;
    Triangle* facet_t;
    std::vector<Triangle>* facets_t;
}
/* Tokens */
%token TOK_EOF 0    "end of file"

%token  ENDFACET    "endfacet"
        ENDLOOP     "endloop"
        ENDSOLID    "endsolid"
        FACET       "facet"
        LOOP        "loop"
        NORMAL      "normal"
        OUTER       "outer"
        SOLID       "solid"
        VERTEX      "vertex"
%token <string> NAME
%token <number> NUMBER

%type <string> solid_start
%type <string> solid_end
%type <facets_t> facets
%type <facet_t> facet
%type <vertex3_t> loop
%type <vertex3_t> vertex3
%type <coord_t> point3
%type <vertex_t> vertex
//%type<std::arrray<double, 3>>

%destructor {delete$$;} <string>
                        <facet_t>
                        <vertex3_t>
                        <coord_t>
                        <vertex_t>
                        <facets_t>

/* Entry point of grammar */
%start object
%%

object:
solid_start facets
solid_end TOK_EOF
        {
                std::string err_msg{ "" };
        if (*$1 != *$3)
        {
            if (*$1 == "")
                std::swap($1, $3);
            else if (*$3 != "") // *$1 != *$3 and *$3 != "" and *$3 != ""
                err_msg = "2 different name(" + *$1 + " and" + *$3 + ")";
        }
        if (!err_msg.length())
        driver.make_object(*$1, *$2);
        delete $1;
        delete $2;
        delete $3;
        if (err_msg.length())
            driver.err(err_msg);
    }
      ;

solid_start: SOLID NAME
    {
        $$ = $2;
    }
           | SOLID
    {
        $$ = new std::string();
    }
           ;

solid_end: ENDSOLID NAME
    {
        $$ = $2;
    }
         | ENDSOLID
    {
        $$ = new std::string();
    }
         ;

facets: facet
    {
        $$ = new std::vector<Triangle>();
        $$->emplace_back(*$1);
        delete $1;
    }
      | facets facet
    {
        $$ = $1;
        $$->emplace_back(*$2);
        delete $2;
    }
      ;

facet: FACET NORMAL point3 loop ENDFACET
    {
        $$ = new Triangle(Vect3Normal(*$3), *$4);
        delete $3;
        delete $4;
    }
     ;

loop: OUTER LOOP vertex3 ENDLOOP
    {
        $$ = $3;
    }
    ;

vertex3: vertex vertex vertex
     {
        $$ = new std::array<Point3, 3>{*$1, *$2, *$3};
        delete $1;
        delete $2;
        delete $3;
     }
       ;

vertex: VERTEX point3
     {
        $$ = new Point3(*$2);
        delete $2;
     }
      ;

point3: NUMBER NUMBER NUMBER
    {
        $$ = new std::array<float, 3>{$1, $2, $3};
    }
      ;
%%
namespace stl::ascii
{
    void Parser::error(const location&, const std::string& m)
    {
        std::cerr << *driver.loc << ": " << m << std::endl;
        driver.error = (driver.error == 127 ? 127 : driver.error + 1);
    }
}
