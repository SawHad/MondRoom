%{
#include "parser.hh"
#include "Scanner.hpp"
#include "Driver.hpp"

/*  Defines some macros to update locations */


#define STEP()                                      \
    do                                              \
    {                                               \
        driver.loc->step();                         \
    } while (0)

#define COL(Col)                                    \
    driver.loc->columns(Col)

#define LINE(Line)                                  \
    do                                              \
    {                                               \
        driver.loc->lines(Line);                    \
    } while (0)

#define YY_USER_ACTION                              \
    COL(yyleng);


typedef stl::ascii::Parser::token token;
typedef stl::ascii::Parser::token_type token_type;

#define yyterminate() return token::TOK_EOF

%}

%option debug
%option c++
%option noyywrap
%option never-interactive
%option yylineno
%option nounput
%option batch
%option prefix="parse"

/*
%option stack
*/

/* Abbreviations.  */

blank   [ \t]+
eol     [\n\r]+

digit           [0-9]
number          {digit}+
exponent        e[+-]?{number}
real            (-?){number}("."{number})?{exponent}?

name            [a-zA-Z]+

%%

 /* The rules.  */
%{
  STEP();
%}

{blank}       STEP();
{eol}         LINE(yyleng);

"facet"     { return token::FACET;     }
"endfacet"  { return token::ENDFACET;  }
"outer"     { return token::OUTER;     }
"loop"      { return token::LOOP;      }
"endloop"   { return token::ENDLOOP;   }
"solid"     { return token::SOLID;     }
"endsolid"  { return token::ENDSOLID;  }
"normal"    { return token::NORMAL;    }
"vertex"    { return token::VERTEX;    }

{name}      {
                yylval->string = new std::string(yytext);
                return token::NAME; 
            }
{real}      {
                std::setlocale(LC_ALL, "en_US.UTF-8");
                yylval->number = std::stof(yytext);
                return token::NUMBER;
            }

.           {
                driver.err("Unexpected token: " + std::string(yytext));
                driver.error = (driver.error == 127 ? 127
                                : driver.error + 1);
                STEP ();
            }
%%


/*
   CUSTOM C++ CODE
*/

namespace stl::ascii
{

    Scanner::Scanner()
    : parseFlexLexer()
    {
    }

    Scanner::~Scanner()
    {
    }

    void Scanner::set_debug(bool b)
    {
        yy_flex_debug = b;
    }
}

#ifdef yylex
# undef yylex
#endif

int parseFlexLexer::yylex()
{
  std::cerr << "call parsepitFlexLexer::yylex()!" << std::endl;
  return 0;
}

