#pragma once
# include "parser.hh"

# ifndef YY_DECL
#  define YY_DECL stl::ascii::Parser::token_type                              \
     stl::ascii::Scanner::yylex(stl::ascii::Parser::semantic_type* yylval,    \
                                stl::ascii::Parser::location_type*,           \
                                stl::ascii::Driver& driver)
# endif

# ifndef __FLEX_LEXER_H
#  define yyFlexLexer parseFlexLexer
#  include <FlexLexer.h>
#  undef yyFlexLexer
# endif


namespace stl::ascii
{
    class Scanner : public parseFlexLexer
    {
    public:
        Scanner();

        virtual ~Scanner();

        virtual Parser::token_type yylex(
                Parser::semantic_type* yylval,
                Parser::location_type* l,
                Driver& driver);

        void set_debug(bool b);
    };
}