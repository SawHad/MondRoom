#include "Driver.hpp"
#include "parser.hh"
#include "Scanner.hpp"

namespace stl::ascii
{
    Driver::Driver()
            : scanner{new Scanner{}},
            parser{new Parser{*this}},
            loc{new location{}}
    {}


    Driver::~Driver()
    {
        delete loc;
    }


    void Driver::reset()
    {
        delete loc;
        loc = new location();
    }

    std::shared_ptr<ObjectStl> Driver::parse()
    {
        scanner->switch_streams(&std::cin, &std::cerr);
        parser->parse();
        return object;
    }

    std::shared_ptr<ObjectStl> Driver::parse_file(const std::string& path)
    {
        std::ifstream s(path.c_str(), std::ifstream::in);
        if (!s.is_open())
            throw std::runtime_error("cannot open file: " + path);
        scanner->switch_streams(&s, &std::cerr);
        parser->parse();

        s.close();
        return object;
    }

    void Driver::err(const std::string& message)
    {
        //parser->error(*loc, message);
        err_message = message;
        throw Parser::syntax_error(*loc, message);
    }

    void Driver::make_object(const std::string& name,
                             const std::vector<Triangle>& triangles)
    {
        object = std::make_shared<ObjectStl>(name, triangles);
    }

}
