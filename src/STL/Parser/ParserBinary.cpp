#include "ParserBinary.hpp"
#include "binary/Parser.hpp"

namespace stl
{
    std::shared_ptr<ObjectStl> ParserBinary::operator()(
            const std::string& path) const
    {
        stl::binary::Parser p;
        return p.parse_file(path);
    }
}