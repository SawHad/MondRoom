#include "ParserAscii.hpp"
#include "ascii/Driver.hpp"

namespace stl
{
    std::shared_ptr<ObjectStl>
    ParserAscii::operator()(const std::string& path) const
    {
        ascii::Driver d;
        return d.parse_file(path);
    }
}