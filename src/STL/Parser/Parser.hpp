#pragma once
#include <memory>
#include "ExceptionStlFile.hpp"
#include "../stl_object/ObjectStl.hpp"
namespace stl
{
    class Parser
    {
    public:
        virtual std::shared_ptr<ObjectStl>
                operator()(const std::string& path) const = 0;

        /// parse file and return stl object
        /// @param path path of file
        /// @thow ExceptionStlFile if file is incorrect
        /// @return
        static std::shared_ptr<ObjectStl> parse(const std::string& path);
    };
}