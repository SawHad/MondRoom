#pragma once
#include "Parser.hpp"
#include "binary/ExceptionAscii.hpp"

namespace stl
{
    class ParserBinary
    {
    public:
        ParserBinary() = default;
        ~ParserBinary() = default;
        virtual std::shared_ptr<ObjectStl>
                    operator()(const std::string& path) const;
    };
}