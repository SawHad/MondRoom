#include "ExceptionStlFile.hpp"

namespace stl
{
    ExceptionStlFile::ExceptionStlFile(const std::string& path)
            : message{path + " is not an correct stl file!"}
    {}

    ExceptionStlFile::~ExceptionStlFile()
    {}

    const char* ExceptionStlFile::what() const noexcept
    {
        return message.c_str();
    }

}